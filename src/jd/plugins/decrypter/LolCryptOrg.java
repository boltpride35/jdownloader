//jDownloader - Downloadmanager
//Copyright (C) 2009  JD-Team support@jdownloader.org
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jd.plugins.decrypter;

import java.util.ArrayList;

import jd.PluginWrapper;
import jd.controlling.ProgressController;
import jd.http.Browser;
import jd.parser.html.Form;
import jd.plugins.CryptedLink;
import jd.plugins.DecrypterPlugin;
import jd.plugins.DownloadLink;
import jd.plugins.PluginForDecrypt;

@DecrypterPlugin(revision = "$Revision: 16337 $", interfaceVersion = 2, names = { "lolcrypt.org" }, urls = { "http://(www\\.)?lolcrypt\\.org/folder\\?fid=[a-z0-9]+" }, flags = { 0 })
public class LolCryptOrg extends PluginForDecrypt {

    public LolCryptOrg(final PluginWrapper wrapper) {
        super(wrapper);
    }

    @Override
    public ArrayList<DownloadLink> decryptIt(final CryptedLink param, final ProgressController progress) throws Exception {
        final ArrayList<DownloadLink> decryptedLinks = new ArrayList<DownloadLink>();
        final String parameter = param.toString();
        br.setFollowRedirects(false);
        br.getPage(parameter);

        /* use cnl2 button if available */
        if (br.containsHTML("127\\.0\\.0\\.1:9666/flash/addcrypted2")) {
            Form cnlform = br.getForm(0);
            if (cnlform != null) {
                // Fehler in Formsyntax --> Form neu bauen!
                for (String[] s : cnlform.getRegex("<INPUT\r?\n?\t?\\s?TYPE=\"hidden\"\r?\n?\t?\\s?NAME=\"(.*?)\"\r?\n?\t?\\s?VALUE=\"(.*?)\">").getMatches()) {
                    cnlform.put(s[0], s[1]);
                }
                final Browser cnlbr = br.cloneBrowser();
                cnlbr.setConnectTimeout(5000);
                cnlbr.getHeaders().put("jd.randomNumber", System.getProperty("jd.randomNumber"));
                try {
                    cnlbr.submitForm(cnlform);
                    if (cnlbr.containsHTML("success")) { return decryptedLinks; }
                } catch (final Throwable e) {
                }
            }
        }
        final String[] links = br.getRegex("\\.\\.(/decrypt\\?fid=[a-z0-9]+\\&lid=\\d+\\&key=[a-z-0-9]+)(\\'|\")").getColumn(0);
        if (links == null || links.length == 0) {
            logger.warning("Decrypter broken for link: " + parameter);
            return null;
        }
        for (final String singleLink : links) {
            br.getPage("http://lolcrypt.org" + singleLink);
            String finallink = br.getRedirectLocation();
            if (finallink == null) {
                logger.warning("Decrypter broken for link: " + parameter);
                return null;
            }
            decryptedLinks.add(createDownloadlink(finallink));
        }
        return decryptedLinks;
    }

}
