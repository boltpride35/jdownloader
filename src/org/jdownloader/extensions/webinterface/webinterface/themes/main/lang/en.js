var dictonary = { 
	"jd.jd"  : "JDownloader",
	"jd.delete"  : "Delete",
	"jd.add"  : "Add",
	"jd.edit"  : "Edit",
	"jd.activate"  : "Activate",
	"jd.deactivate"  : "Deactivate",
	"jd.renew"  : "Renew",
	"jd.options"  : "Options",
	
	
	"jd.speed"  : "Downloadspeed",
	"jd.speed.bps"  : "B/s",
	"jd.speed.kps"  : "KB/s",
	"jd.speed.mps"  : "MB/s",
	"jd.speed.gps"  : "GB/s",
	
	"nav.dashboard"  : "Dashboard",
	"nav.downloads"  : "Downloads",
	"nav.grabber"  : "Linkgrabber",
	"nav.passwords"  : "Passwordlist",
	"nav.accounts"  : "Accounts",
	"nav.settings"  : "Settings",
	
	"dashboard.reports.successfull" : "Successfull",
	"dashboard.reports.failed" : "Error Messages",
	"dashboard.reports.done" : "Finish in",
	"dashboard.reports.accounts" : "Active Accounts",
	
	
	"dashboard.downloads.title" : "Download Overview",
	"dashboard.downloads.all" : "Downloads in the List",
	"dashboard.downloads.extracted" : "Extracted",
	"dashboard.downloads.fail_extracted" : "Incorrect Extracted",
	"dashboard.downloads.done" : "Successfull",
	"dashboard.downloads.downloading" : "Downloading",
	"dashboard.downloads.fail" : "Failed",
	
	
	"accounts.reports.title" : "Account",
	"accounts.reports.buy" : "Buy",
	
	"accounts.account.title" : "Accounts",
	"accounts.account.hoster" : "Hoster",
	"accounts.account.user" : "Username",
	"accounts.account.expire" : "Expiredate",
	"accounts.account.status" : "Status",
	"accounts.account.traffic" : "Left Traffic",
	"accounts.account.active" : "Active?",
	
	
	
	"downloads.reports.start" : "Start",
	"downloads.reports.pause" : "Pause",
	"downloads.reports.stop" : "Stop",
	"downloads.reports.update" : "Update",
	"downloads.reports.shutdown" : "Shutdown",
	
	"downloads.download.title" : "Downloads",
	"downloads.download.package" : "Packagename",
	"downloads.download.size" : "Size",
	"downloads.download.hoster" : "Hoster",
	"downloads.download.progress" : "Progress",
	
	
	"captcha.title" : "Captcha",
	"captcha.closed" : "This Captcha was aborted.",
	"captcha.send" : "Solve",
}
$.i18n.setDictionary(dictonary);